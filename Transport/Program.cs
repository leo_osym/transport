﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    class Program
    {
        static void Main(string[] args)
        {
            Horse Bella = new Horse("Bella the Horse", 15, 80, 3);
            Bella.distance = 100;
            Bella.Go();
            Bella.haveARest(8);
            Bella.eatStuff();
            Bella.Go();

            Car John = new Car("John", 120, 100, 10);
            John.distance = 1000;
            John.Go();

            Bicycle Sania = new Bicycle("Sania", 15, 80, 3);
            Sania.distance = 200;
            Sania.Go();
            Sania.eatStuff();
            Sania.Go();

            PassengersCar Leo = new PassengersCar("Passenger Car",100,80,12);
            Leo.NumOfPassengers = 6;
            Leo.distance = 1000;
            Leo.Go();
            Console.ReadKey();

        }
    }
}
