﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    interface ITransport
    {
        int time { get; set; }
        void Go();
    }
}
