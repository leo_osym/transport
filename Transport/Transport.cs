﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    abstract class Transport : ITransport
    {
        public Transport(string name, int speed, int weight, int energy)
        {
            this.speed = speed;
            this.energy = energy;
            this.weight = weight;
            this.name = name;
            Distance = distance;
            distance = 0;
        }

        protected string name;
        protected int speed;
        protected int energy;
        protected int SetDistance;
        protected int weight;
        

        public int distance
        {
            get => SetDistance;
            set => SetDistance = value;
        }
        public int passed;
        public int time { get; set; }
        protected int Distance;
        public int Time;



        virtual public void Go()
        {
            if (SetDistance > 0)
            {
                Distance = SetDistance;
                Console.WriteLine($"{name} is going on {Distance} km!");
                time = 1;
                passed = 0;
                while (Distance > 0 || energy > 0)
                {
                    if (energy > 0)
                    {
                        passed = (speed * time * 100) / weight;
                        if (Distance - passed > 0)
                        {
                            Distance = Distance - passed;
                        }
                        else
                        {
                            Distance = 0;
                        }
                        Console.WriteLine($"We passed {passed} km for {time} hours and have {energy} pt of energy and {Distance} km  left");
                        if (Distance == 0)
                        {
                            Console.WriteLine($"We passed THE WHOLE DISTANCE and have {energy} pt of energy left");
                            break;
                        }
                        time++;
                        Time = time;
                        energy--;
                    }
                    else
                    { Console.WriteLine($"We don't have energy to keep going! We have {Distance - passed} km to go."); break; }
                    SetDistance = Distance - passed;
                }
            }

        }
    }
}
