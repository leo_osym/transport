﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    class Plane : MechTransport
    {
        public Plane(string name, int speed, int weight, int energy)
            : base(name, speed, weight, energy)
        {
        }
        public override void reFuel(int points)
        {
            energy += points;
        }
    }
}
