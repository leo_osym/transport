﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    abstract class MechTransport : Transport
    {
        public MechTransport(string name, int speed, int weight, int energy)
            : base(name, speed, weight, energy)
        {
        }

        abstract public void reFuel(int points);

    }
}
