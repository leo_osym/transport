﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    class PassengersCar : Car, IPassengers
    {
        public PassengersCar(string name, int speed, int weight, int energy)
            : base(name, speed, weight, energy)
        {
            //count every passenger as 80 kilo man
        }

        public int NumOfPassengers { get; set; }

        public override void Go()
        {
            if (SetDistance > 0)
            {
                Distance = SetDistance;
                Console.WriteLine($"{name} is going on {Distance} km!");
                time = 1;
                passed = 0;
                while (Distance > 0 || energy > 0)
                {
                    if (energy > 0)
                    {
                        passed = (speed * time * 100) / (weight * NumOfPassengers * 80 / 250);
                        if (Distance - passed > 0)
                        {
                            Distance = Distance - passed;
                        }
                        else
                        {
                            Distance = 0;
                        }
                        Console.WriteLine($"We passed {passed} km for {time} hours and have {energy} pt of energy and {Distance} km  left");
                        if (Distance == 0)
                        {
                            Console.WriteLine($"We passed THE WHOLE DISTANCE and have {energy} pt of energy left");
                            break;
                        }
                        time++;
                        Time = time;
                        energy--;
                    }
                    else
                    { Console.WriteLine($"We don't have energy to keep going! We have {Distance - passed} km to go."); break; }
                    SetDistance = Distance - passed;
                }
            }

        }

    }
}
