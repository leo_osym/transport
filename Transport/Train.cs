﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    class Train : MechTransport
    {
        public Train(string name, int speed, int weight, int energy)
            : base(name, speed, weight, energy)
        {
        }
        public override void reFuel(int points)
        {
            //energy += points;
            Console.WriteLine("Train doesn't use fuel!");
        }
    }
}
