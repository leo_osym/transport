﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    class Bicycle : MusquleTransport
    {
        public Bicycle(string name, int speed, int weight, int energy)
            : base(name, speed, weight, energy)
        {
        }

        public override void eatStuff()
        {
            Console.WriteLine($"Eat some stuff");
            energy += 4;
            time++;
        }

        public override void haveARest(int hours)
        {
            Console.WriteLine($"Having some rest for {hours} hours");
            energy += hours / 2;
            time += hours;
        }

        public void SelfieToInstagram()
        {
            Console.WriteLine("TAKING A SELFIE!");
            time++;
        }
    }
}
